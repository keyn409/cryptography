#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <math.h>

unsigned long long ipow(unsigned long long base, unsigned long long exp)
{
    unsigned long long result = 1;

    while(1)
    {
        if (exp & 1)
		{
            result *= base;
		}

        exp >>= 1;

        if (0 == exp)
		{
            break;
		}

        base *= base;
    }

    return result;
}

// y = a^x mod p, find x based on Brute force algorithm
void brute_forse(unsigned long long const y, unsigned long long const a, unsigned long long const p)
{
	unsigned long long x = 0;
	unsigned long long ret = 0;

	for (x = 0; x < ULLONG_MAX; x++)
	{
		if (ipow(a, x) % p == y)
		{
			printf("Answer: x = %lld\n", x);

			ret = ipow(a, x) % p;

			if (ret != y)
			{
				printf("Error, calculated result(%lld) != y(%lld)\n", ret, y);
			}
			else
			{
				printf("Calculated result(%lld) == y(%lld)\n", ret, y);
			}

			break;
		}
	}

	if (ULLONG_MAX == x)
	{
		printf("Can't find x for input parameters y = %lld, a = %lld, p = %lld\n", y, a, p);
	}
}

int main(void)
{
	unsigned long long ret = 0;
	unsigned long long const y = 122;
	unsigned long long const a = 79;
	unsigned long long const p = 263;

	brute_forse(y, a, p);

    return ret;
}