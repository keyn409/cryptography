#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

unsigned long long ipow(unsigned long long base, unsigned long long exp)
{
    unsigned long long result = 1;

    while(1)
    {
        if (exp & 1)
		{
            result *= base;
		}

        exp >>= 1;

        if (0 == exp)
		{
            break;
		}

        base *= base;
    }

    return result;
}

// y = a^x mod p, find x based on Shenks algorithm
unsigned long long shenks(unsigned long long const y, unsigned long long const a, unsigned long long const p)
{
	unsigned long long ret = 0;
	unsigned long long *first_row = NULL;
	unsigned long long second_row_elem = 0;
	unsigned long long m = 0;
	unsigned long long k = 0;
	unsigned long long temp = p;
	unsigned long long i = 0;
	unsigned long long j = 0;
	bool is_find_equal = false;
	unsigned long long x = 0;

	// 1. Find m and k values

	while(1)
	{
		temp++;

		for (i = 9; i > 1; i--)
		{
			if (  0 == temp % i
			   && i != temp)
			{
				m = i;
				k = temp / i;
				break;
			}
		}

		if (0 != k)
		{
			break;
		}
	}

	printf("1. Find m and k. p = %lld => m = %lld, k = %lld\n", p, m, k);

	// 2. Allocate memory only for ont row

	first_row = calloc(m, sizeof(*first_row));
	if (NULL == first_row)
	{
		printf("%s(%d): calloc() failed", __func__, __LINE__);
		ret = -1;
		goto finally;
	}

	printf("2. Calculate values of first row: ");

	// 3. Calculate values on first row (a^(m-1) * y) % p
	for (i = 0; i < m; i++)
	{
		first_row[i] = ipow(a, i) * y % p;
		printf("%lld, ", first_row[i]);
	}

	printf("\n3. Calculate values of second row: ");

	// 4. Calculate values on second row (a^(k * m)) and find equal elements

	for (j = 1; j <= k; j++)
	{
		second_row_elem = ipow(a, j * m) % p;

		printf("%lld, ", second_row_elem);

		for (i = 0; i < m; i++)
		{
			if (first_row[i] == second_row_elem)
			{
				printf("\n4. Find equal elements, i = %lld, j = %lld\n", i, j);
				is_find_equal = true;
				break;
			}
		}

		if (true == is_find_equal)
		{
			break;
		}
	}

	if (false == is_find_equal)
	{
		printf("\nExecution error, no equal elements\n");
		ret = -2;
		goto finally;
	}

	// 5. Calculate x

	x = (j) * m - i;

	printf("5. Answer: x = %lld\n", x);

	ret = ipow(a, x) % p;

	if (ret != y)
	{
		printf("Error, calculated result(%lld) != y(%lld)\n", ret, y);
	}
	else
	{
		printf("Calculated result(%lld) == y(%lld)\n", ret, y);
	}

 finally:
	free(first_row);
	return ret;
}

int main(void)
{
	unsigned long long ret = 0;
	unsigned long long const y = 122;
	unsigned long long const a = 79;
	unsigned long long const p = 263;

	shenks(y, a, p);

    return ret;
}