#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define TEXT_MAX_LEN 10000
#define FILE_OUTPUT "result.txt"

void vernam_encription_unsafe(
	char const *const input_text,
	char const *const key,
	char *const output_text)
{
	size_t text_len = strlen(input_text);

	for (size_t i = 0; i < text_len; i++)
	{
		if (input_text[i] == '\n' || output_text[i] == ' ')
		{
			continue;
		}

		output_text[i] = input_text[i] ^ key[i];
	}
}

int main(int const argc, char const *argv[])
{
	int ret = 0;
	FILE *input_file = NULL;
	FILE *file_with_key = NULL;
	FILE *output_file = NULL;
	char input_text[TEXT_MAX_LEN] = {0};
	char key[TEXT_MAX_LEN] = {0};
	char output_text[TEXT_MAX_LEN] = {0};

	if (argc < 3)
	{
		printf("Usage: %s file_for_encription file_with_key\n", argv[0]);
		ret = -1;
		goto finally;
	}

	input_file = fopen(argv[1], "r");
	if (NULL == input_file)
	{
		printf("fopen(input_file) failed, %s\n", strerror(errno));
		ret = -2;
		goto finally;
	}

	file_with_key = fopen(argv[2], "r");
	if (NULL == file_with_key)
	{
		printf("fopen(file_with_key) failed, %s\n", strerror(errno));
		ret = -3;
		goto finally;
	}

	fread(input_text, sizeof(char), TEXT_MAX_LEN, input_file);
	ret = ferror(input_file);
	if (0 != ret)
	{
		printf("Error while reading file for encription, return result = %d\n", ret);
		ret = -4;
		goto finally;
	}

	fread(key, sizeof(char), TEXT_MAX_LEN, file_with_key);
	ret = ferror(file_with_key);
	if (0 != ret)
	{
		printf("Error while reading file with key, return result = %d\n", ret);
		ret = -5;
		goto finally;
	}

	if (strlen(input_text) != strlen(key))
	{
		printf("Input text and key should have equal len!\n");
		ret = -6;
		goto finally;
	}

	vernam_encription_unsafe(input_text, key, output_text);

	output_file = fopen(FILE_OUTPUT, "w+");
	if (NULL == output_file)
	{
		printf("fopen() failed, %s\n", strerror(errno));
		ret = -4;
		goto finally;
	}

	fputs(output_text, output_file);

 finally:
	if (NULL != input_file)
	{
		fclose(input_file);
	}

	if (NULL != output_file)
	{
		fclose(output_file);
	}
	return ret;
}
