#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gcrypt.h>
#include <stdbool.h>

enum
{
    MAX_DATA_LEN = 128,
    KEY_LEN = 128/8,
};

static int aes128_unsafe(char const *const input_text, size_t text_size)
{
    char const key[KEY_LEN] = "very secret key";
    // unsigned char const input_text[] = "some text_11234";
    gcry_cipher_hd_t hd;
    unsigned char result[MAX_DATA_LEN];
    int ret = 0;
    bool is_lib_inited = false;

    // Initialize crypt library and algorithm
    ret = gcry_cipher_open(&hd, GCRY_CIPHER_AES, GCRY_CIPHER_MODE_CBC, GCRY_CIPHER_CBC_CTS);
    if (0 != ret)
    {
        printf("gcry_cipher_open() failed\n");
        ret = -1;
        goto finally;
    }

    is_lib_inited = true;

    // Set key
    ret = gcry_cipher_setkey(hd, key, sizeof(key));
    if (0 != ret)
    {
        printf("gcry_cipher_setkey() failed\n");
        ret = -2;
        goto finally;
    }

    // Set the initialization vector
    ret = gcry_cipher_setiv(hd, NULL, 0);
    if (0 != ret)
    {
        printf("gcry_cipher_setiv() failed\n");
        ret = -3;
        goto finally;
    }

    // Encrypt input text
    ret = gcry_cipher_encrypt(hd, result, MAX_DATA_LEN, input_text, text_size);
    if (0 != ret)
    {
        printf("gcry_cipher_encrypt() failed\n");
        ret = -4;
        goto finally;
    }

    printf("AES128 encrypt result - %s\n", result);

    // Set the initialization vector again
    ret = gcry_cipher_setiv(hd, NULL, 0);
    if (0 != ret)
    {
        printf("gcry_cipher_setiv() failed\n");
        ret = -5;
        goto finally;
    }

    // Decrypt encription result
    ret = gcry_cipher_decrypt(hd, result, sizeof(result), NULL, 0);
    if (0 != ret)
    {
        printf("gcry_cipher_decrypt() failed\n");
        ret = -6;
        goto finally;
    }

    printf("AES128 decryption result - %s\n", result);

 finally:

    if (true == is_lib_inited)
    {
        gcry_cipher_close(hd);
    }

    return ret;
}

int main(int argc, char *argv[])
{
    int ret = 0;
    char input_text[KEY_LEN] = {0};

    if (2 != argc)
    {
        printf("Usage: %s text_to_encrypt, max length - %lu\n", argv[1], sizeof(input_text) - 1);
        ret = -1;
        goto finally;
    }

    ret = snprintf(input_text, sizeof(input_text), "%s", argv[1]);
    if (ret <= 0 || ret >= (int)sizeof(input_text))
    {
        printf("Incorrect length of input text(%lu), max avaliable length for one block is %lu\n",
            strlen(argv[1]), sizeof(input_text) - 1);
        ret = -2;
        goto finally;
    }

    ret = aes128_unsafe(input_text, sizeof(input_text));
    if (0 != ret)
    {
        printf("aes128_unsafe() failed, return result = %d\n", ret);
        ret = -3;
        goto finally;
    }

finally:
    return ret;
}