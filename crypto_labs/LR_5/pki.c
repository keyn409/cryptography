#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

typedef enum binary_tree_consts
{
    BIN_TREE_ELEM_MAX_VALUE = 10000000,
	MAIN_CENTER_OPEN_KEY = 1234,
	MAIN_CENTER_PRIVATE_KEY = 9876,
	PRIVATE_SAULT = 1000,
	CENTERS_COUNT = 7,
	USERS_COUNT = 8,
	MAX_LEN = 250,

	// Constants for tests
	USER_TEST_NUMBER = 3,
	CENTER_TEST_NUMBER = 4,
	USER3_GOOD_OPEN_KEY = 24884,
	USER_WRONG_OPEN_KEY = 10000000,
	USER_WRONG_PRICATE_KEY = 404,
} binary_tree_consts;

typedef struct Certification_center Certification_center;

// Every certification center can have two childs
struct Certification_center
{
	int parent_center_number;
	int center_number;
	int open_key;
	int private_key;
	int child_open_key[2];
	int child_private_key[2];
};

// Certificate contain a open key - public information
typedef struct Certificate
{
	int version;
	int center_number;
	int user_number;
	int open_key;
} Certificate;

// User contain a private key - closed information
typedef struct User
{
	int user_number;
	int private_key;
	struct Certificate certificate;
} User;


void create_child_certificate_center(
	struct Certification_center *parent_center,
	struct Certification_center *child_center)
{
	// Calculate private key

	child_center->private_key = parent_center->private_key - PRIVATE_SAULT + child_center->center_number;

	// Calculate open key based on private key
	child_center->open_key = child_center->private_key + parent_center->open_key + child_center->center_number;

	// Save keys

	if (child_center->center_number % 2 == 0)
	{
		parent_center->child_private_key[1] = child_center->private_key;
		parent_center->child_open_key[1] = child_center->open_key;
	}
	else
	{
		parent_center->child_private_key[0] = child_center->private_key;
		parent_center->child_open_key[0] = child_center->open_key;
	}

	child_center->parent_center_number = parent_center->center_number;
	child_center->child_open_key[0] = -1;
	child_center->child_private_key[0] = -1;
	child_center->child_open_key[1] = -1;
	child_center->child_private_key[1] = -1;

	printf("Center %d - private key - %d, open key - %d\n", child_center->center_number, child_center->private_key, child_center->open_key);
}


int create_certification_center(
	struct Certification_center **binary_tree,
	int tree_size)
{
	int ret = 0;

	for (int i = 0; i < tree_size; i++)
	{
		if (NULL != binary_tree[i])
		{
			continue;
		}

		binary_tree[i] = calloc(1, sizeof(*binary_tree[i]));
		if (NULL == binary_tree[i])
		{
			printf("calloc() failed - %s", strerror(errno));
			ret = -1;
			goto finally;
		}

		binary_tree[i]->center_number = i;

		// If create a main certificate center
		if (0 == i)
		{
			binary_tree[0]->open_key = MAIN_CENTER_OPEN_KEY;
			binary_tree[0]->private_key = MAIN_CENTER_PRIVATE_KEY;
			binary_tree[0]->child_open_key[0] = -1;
			binary_tree[0]->child_private_key[0] = -1;
			binary_tree[0]->child_open_key[1] = -1;
			binary_tree[0]->child_private_key[1] = -1;
		}
		else
		{
			int parent_center_index = 0;

			// Identify index of paretn center
			if (0 == i % 2)
			{
				parent_center_index = (i - 2) / 2;
			}
			else
			{
				parent_center_index = (i - 1) / 2;
			}

			create_child_certificate_center(binary_tree[parent_center_index], binary_tree[i]);
		}
	}

 finally:
	errno = 0;
	return ret;
}

void create_certificate_for_user(
	struct User *user,
	int user_index,
	struct Certification_center *parent_center)
{
	user->user_number = user_index;
	user->certificate.center_number = parent_center->center_number;

	// Calculate private key

	user->private_key = parent_center->private_key - PRIVATE_SAULT + user->user_number;

	// Calculate open key based on private key
	user->certificate.open_key = user->private_key + parent_center->open_key + user->user_number;

	// Save keys

	if (user->user_number % 2 == 0)
	{
		parent_center->child_private_key[1] = user->private_key;
		parent_center->child_open_key[1] = user->certificate.open_key;
	}
	else
	{
		parent_center->child_private_key[0] = user->private_key;
		parent_center->child_open_key[0] = user->certificate.open_key;
	}

	user->certificate.version = 1;

	printf("User %d - private key - %d, open key - %d\n", user->user_number, user->private_key, user->certificate.open_key);
}

void add_user_to_certification_center(
	struct Certification_center **binary_tree,
	int binary_tree_size,
	struct User *user,
	int user_index)
{
	bool is_left_user_exist = false;
	bool is_right_user_exist = false;

	for (int i = 0; i < binary_tree_size; i++)
	{
		is_left_user_exist = (-1 != binary_tree[i]->child_open_key[0]);
		is_right_user_exist = (-1 != binary_tree[i]->child_open_key[1]);

		if (true == is_left_user_exist && true == is_right_user_exist)
		{
			continue;
		}

		create_certificate_for_user(user, user_index, binary_tree[i]);

		break;
	}
}

int verify_center_keys(
	struct Certification_center **binary_tree,
	struct Certification_center child_center,
	char *trusted_chain)
{
	int ret = 0;
	int calculated_user_key = 0;
	int key_array_index = 0;
	struct Certification_center *parent_center = binary_tree[child_center.parent_center_number];

	// If there is a main certification center, we don't need to verify keys
	if (child_center.center_number == 0)
	{
		char tmp[MAX_LEN] = {0};
		snprintf(tmp, MAX_LEN, "Center #%d\n", child_center.center_number);
		strcat(trusted_chain, tmp);
		goto finally;
	}

	// Identify key array index based on center number
	if (child_center.center_number % 2 == 0)
	{
		key_array_index = 1;
	}
	else
	{
		key_array_index = 0;
	}

	// Check user open key
	if (child_center.open_key != parent_center->child_open_key[key_array_index])
	{
		printf("Center #%d open key isn't correct!\n", child_center.center_number);
		goto finally;
	}

	// Calculate center private key based on used public key
	calculated_user_key = child_center.open_key - parent_center->open_key - child_center.center_number;

	// Check center private key
	if (  calculated_user_key != parent_center->child_private_key[key_array_index]
	   || parent_center->child_private_key[key_array_index] != child_center.private_key)
	{
		printf("Center #%d private key isn't correct!\n", child_center.center_number);
		goto finally;
	}

	// Save result to trusted_chain
	{
		char tmp[MAX_LEN] = {0};
		snprintf(tmp, MAX_LEN, "Center #%d -> ", child_center.center_number);
		strcat(trusted_chain, tmp);
	}

	verify_center_keys(binary_tree, *parent_center, trusted_chain);

 finally:
	return ret;
}

int veryfi_user_keys(
	struct Certification_center **binary_tree,
	struct User user)
{
	int ret = 0;
	int calculated_user_key = 0;
	int key_array_index = 0;
	struct Certification_center *parent_center = binary_tree[user.certificate.center_number];
	char trusted_chain[MAX_LEN] = {0};

	// Identify key array index based on user number
	if (user.user_number % 2 == 0)
	{
		key_array_index = 1;
	}
	else
	{
		key_array_index = 0;
	}

	// Check user open key
	if (user.certificate.open_key != parent_center->child_open_key[key_array_index])
	{
		printf("User #%d open key isn't correct!\n", user.user_number);
		goto finally;
	}

	// Calculate user private key based on used public key
	calculated_user_key = user.certificate.open_key - parent_center->open_key - user.user_number;

	// Check user private key
	if (  calculated_user_key != parent_center->child_private_key[key_array_index]
	   || parent_center->child_private_key[key_array_index] != user.private_key)
	{
		printf("User #%d private key isn't correct!\n", user.user_number);
		goto finally;
	}

	printf("Keys of user #%d verified\n", user.user_number);

	snprintf(trusted_chain, MAX_LEN, "Trust chain: User #%d -> ", user.user_number);

	ret = verify_center_keys(binary_tree, *parent_center, trusted_chain);
	if (0 != ret)
	{
		goto finally;
	}

	// Show tructed chae
	printf("%s", trusted_chain);

 finally:
	return ret;
}

int main(void)
{
	int ret = 0 ;
	struct Certification_center *binary_tree[CENTERS_COUNT] = {0};
	struct User users[USERS_COUNT] = {0};

	// fill array of certification centers
	ret = create_certification_center(binary_tree, CENTERS_COUNT);

	for (int i = 0; i < USERS_COUNT; i++)
	{
		add_user_to_certification_center(binary_tree, CENTERS_COUNT, &users[i], i);
	}

	for (int i = 0; i < USERS_COUNT; i++)
	{
		veryfi_user_keys(binary_tree, users[i]);
	}

	printf("\nCheck work with incorrect users:\n");

	// Try to verify user with incorrect open key
	{
		struct User user = {.certificate.open_key = 12345};
		user.user_number = 6;
		veryfi_user_keys(binary_tree, user);
	}

	// Try to verify user with incorrect private key
	{
		struct User user = {.certificate.open_key = USER3_GOOD_OPEN_KEY,
							.certificate.center_number = CENTER_TEST_NUMBER,
							.private_key = USER_WRONG_PRICATE_KEY};
		user.user_number = USER_TEST_NUMBER;
		veryfi_user_keys(binary_tree, user);
	}

    return ret;
}